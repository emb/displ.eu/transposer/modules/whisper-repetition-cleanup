# Whisper Repetition Cleanup

A collection of cleanup functions for Whisper's transcript objects.

As Whisper tends to repeat words or parts of sentences when there's music or when it can't recognize the upcoming speech, we need to cleanup both the plain transcript as well as the JSON object of segments and timestamped words returned by Whisper.

Since we use transcripts for full text and vector search this will otherwise drastically distort the search results.

The script detects different forms of text repetitions, removes them based on the configuration and returns the cleaned up Whisper JSON object.

# Modules

## repetitions_finder.py

Finds, showcases and corrects the repetitions in a given text. Repetition is defined as a string that has the same string before it and after it. Detailed explanations are inside the file.

### Requirements: 

`!pip install nltk`

### Passable arguments (default):

* punctuation_token ("ⓟ")
* replacement_token ("ⓣ")
* color ("red")
* allow_repetitions (2)
* suspicious_length (10)
* n_grams_span (1, 50)

### Callable functions

#### preprocess:

Lowers and replaces punctuation

#### find_repetitions 

Finds both merged and spaced repetitions by calling <br>
Find_merged_repetitions and find_spaced_repetitions

#### find_spans

Determines at what indices the repetitions are.

#### pad_text

Replaces repetitions with tokens.

#### clean_text

Returns a fiven text without repetitions.

#### clean_segments

Returns the segments and words JSON object without repetions.

#### clean

Returns a text or a JSON without repetitions.

#### showcase

Shows repetitions percent, the repetitions, original text with marked repetitions.

## language_checker.py

Determines the language of the text using three different models, compares it to the given language.

### Requirements

`!pip install langid`
`!pip install guess_language-spirit`
`!pip install langdetect`

### Callable functions

#### check_language

Returns dictionary with original language, three language guesses, correctness score:

* 2 - all languages match
* 1 - at least one guessed language matches the original
* 0 - no guessed languages match the original
* -1 - two models cannot determine the language of the text (the text is not valid)

# Authors

Polina Poliaeva